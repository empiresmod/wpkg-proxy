package main

import "fmt"
import "golang.org/x/crypto/openpgp"
import "net/http"
import "strings"
import "os"
import "log"
import "io"
import "bytes"
import "time"
import "errors"
import "strconv"
import "os/exec"

func handler(keyring openpgp.EntityList, w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Connection", "close")

	parts := strings.Split(r.URL.Path[1:], "/")
	hostname := parts[0]
	rest := strings.Join(parts[1:], "/")

	url := fmt.Sprintf("http://%s/%s", hostname, rest)
	sigurl := fmt.Sprintf("%s.asc", url)

	file, fileError := http.Get(url)
	signature, signatureError := http.Get(sigurl)

	if fileError != nil || signatureError != nil {
		w.WriteHeader(http.StatusInternalServerError)
		if fileError != nil {
			fmt.Fprintf(w, "An error occured downloading the file: %s", fileError.Error())
		}
		if signatureError != nil {
			fmt.Fprintf(w, "An error occured downloading the signature: %s", signatureError.Error())
			fmt.Printf("An error occured downloading the signature: %s", signatureError.Error())
		}
		return
	}

	if file.StatusCode != 200 {
		w.Header().Add("Content-Type", "text/html")
		w.WriteHeader(file.StatusCode)
		fmt.Fprintf(w, "Unable to fetch <a href=\"%s\">requested file file</a>", url)
		fmt.Printf("Unable to fetch %s", url)
		return
	}

	if signature.StatusCode != 200 {
		w.Header().Add("Content-Type", "text/html")
		w.WriteHeader(http.StatusFailedDependency)
		fmt.Fprintf(w, "Unable to fetch <a href=\"%s\">signature file</a>", sigurl)
		fmt.Printf("Unable to fetch %s", sigurl)
		return
	}

	var buffer bytes.Buffer
	var splitter = io.TeeReader(file.Body, &buffer)

	_, err := openpgp.CheckArmoredDetachedSignature(keyring, splitter, signature.Body)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "Unable to verify signature:\n%s", err.Error())
		fmt.Printf("Unable to verify %s", sigurl)
		return
	}

	w.Header().Add("Content-Length", strconv.Itoa(buffer.Len()))
	w.Header().Add("Content-Type", file.Header["Content-Type"][0])

	io.Copy(w, &buffer)

	r.Close = true
}

func main() {
	if len(os.Args) < 3 {
		log.Fatal(errors.New("Not enough arguments. Usage: wpkg-proxy keyring wpkg-command [arguments...]"))
	}

	path := os.Args[1]
	keyfile, err := os.Open(path)

	if err != nil {
		log.Fatal(err)
	}

	keyring, err := openpgp.ReadKeyRing(keyfile)
	if err != nil {
		log.Fatal(err)
	}

	bound := func(w http.ResponseWriter, r *http.Request) {
		handler(keyring, w, r)
	}
	http.HandleFunc("/", bound)

	go func() {
		log.Fatal(http.ListenAndServe("127.0.0.1:26999", nil))
	}()

	time.Sleep(100 * time.Millisecond)

	cmd := exec.Command(os.Args[2])
	cmd.Args = os.Args[2:]
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Run()
}
