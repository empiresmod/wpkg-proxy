all: windows linux

windows: wpkg-proxy.exe

linux: wpkg-proxy

wpkg-proxy.exe: wpkg-proxy.go
	GOOS=windows GOARCH=386 CC=i686-w64-mingw32-gcc go build -ldflags "-linkmode external -extldflags -static" wpkg-proxy.go

wpkg-proxy: wpkg-proxy.go
	GOOS=linux GOARCH=386 go build -ldflags "-linkmode external -extldflags -static" wpkg-proxy.go
